https://mattafaw@bitbucket.org/mattafaw/randstad_test_project.git# README #

Randstad Technical Exercise for QA - done by Ahmad Farouq Bin Abdul Wahab

## What is this repository for? ##

* Quick summary: This is a repository containing answers for Randstad automation test, done by me. 
* The first scenarios covers on opening a randstad page click apply with us and view the page
* The second scenario is to try to search for consultant jobs available 
* Version: 1.0

## Testing ##
### Dependencies and Assumptions ###

* The test will open a browser to https://www.randstad.com.au and find a text 'to join our team.  
* The test then will be click apply with us and view the job postings page
* The 2nd scenarios will basically insert a data "consultant" and search the job application available
* The test has been written under Eclipse as a Maven application, on Windows 7 platform, using Java 1.8 and Selenium WebDriver version 3.141.59.
* Ensure that Maven can be executed from your path, and Chrome browser is updated (*tested with version 76.0.3809.87*) and configured without any proxy. 
* At times the findElements might not be detected by id due to it's incompatible when using chromedriver, so a little intervention to change to xpath

### How do I get set up? ###


* Clone this repository: git clone https://mattafaw@bitbucket.org/mattafaw/randstad_test_project.git 
* Go to the cloned folder: cd /path/to/atlassian-tech-exercise
* Run test: mvn clean test
* See the test running automatically!

