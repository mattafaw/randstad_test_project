package Testcase;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Scenario_2_view_consultant_result {
	public static void assertTrue(boolean contains) {

	}

	WebDriver driver = null;
	WebElement textbox = null;
	
	@BeforeTest
	public void SetupTest() {
		String projectPath = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", projectPath + "\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test
	public void ViewConsultantResultPage() {
	
		// go to page
		driver.get("https://www.randstad.com.au");

		// during loading, find message "join our team
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		String message1 = driver.findElement(By.xpath("//div[@id='ctl15_OneColumnInnerDiv']/div/div[6]/div/div[2]/h2")).getText();
		String expectedmessage1 = "join our team.";
		if (message1.equals(expectedmessage1)) {
			System.out.println("Message join our team matched");
		}
		else {
			System.out.println("Message join our team not matched");
		}
		
		System.out.println("Success");

		// click apply with us button
		driver.findElement(By.xpath("//a[contains(text(),'apply with us')]")).click();
		System.out.println("success to click");
		
		//finding header job postings displayed
		String dataheader = driver.findElement(By.xpath("//div[@id='ctl08_ctl05_LayoutDiv']/header/div/div/h1")).getText();
		String expectedheader = "job postings\nsearch our jobs online";
		if (dataheader.equals(expectedheader)) {
			System.out.println("Results job postings header matched");
		}
		else {
			System.out.println("Results job postings header not matched");
		}

		//view label what is displayed
		String message2 = driver.findElement(By.xpath("//div[2]/div/div/div/div/span")).getText();
		String expectedmessage2 = "what?";
		if (message2.equals(expectedmessage2)) {
			System.out.println("Label 'what?' is matched");
		}
		else {
			System.out.println("Label 'what?' not matched");
		}
		
		//insert consultant in textboxt and click submit
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    driver.findElement(By.xpath("//div[@id='ctl08_ctl05_SearchWrapper']/div/div/input")).sendKeys("consultant");
	    
	    //click submit button
	    driver.findElement(By.id("ctl08_ctl05_WhatWhereSubmitLinkButton")).click();;
	    
	  //view header result page displayed
	  	String headerresult = driver.findElement(By.xpath("//div[@id='ctl08_ctl05_LayoutDiv']/header/div/div/h1")).getText();
	  	String expectedheaderresult = "job postings\nconsultant jobs";
	  	if (headerresult.equals(expectedheaderresult)) {
	  		System.out.println("Header consultant result is matched");
	  	}
	  	else {
	  		System.out.println("Header consultant result not matched");
	  	}
	  	
	    //view total number of jobs available	  
		String message3 = driver.findElement(By.xpath("//div[@id='ctl08_ctl05_NrOfJobsSortByDiv']/h3/span")).getText();
		String expectedmessage3 = "jobs 1 to 10 of 28";
	  	if (message3.equals(expectedmessage3)) {
	  		System.out.println("Total jobs for consultant result is matched");
	  	}
	  	else {
	  		System.out.println("Total jobs for consultant result not matched");
	  	}
			    
	}
}
