package Testcase;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Scenario_1 {
	private static void assertTrue(boolean contains) {

	}
	WebDriver driver = null;
	
	@BeforeTest
	public void SetupTest() {
		String projectPath = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", projectPath + "\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test
	public void ViewSearchPage() {
		//go to page
		driver.get("https://www.randstad.com.au");
		
		//during loading, find message "join our team
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		String str = driver.findElement(By.xpath("//div[@id='ctl15_OneColumnInnerDiv']/div/div[6]/div/div[2]/h2"))
				.getText();
		assertTrue(str.contains("join our team."));
		System.out.println("Success");
		
		//click apply with us button
		driver.findElement(By.xpath("//a[contains(text(),'apply with us')]")).click();
		System.out.println("success to click");
		
		//view page and display job postings search our jobs online
		String page = driver.findElement(By.xpath("//div[@id='ctl08_ctl05_LayoutDiv']/header/div/div/h1")).getText();
		assertTrue(page.contains("job postings\\nsearch our jobs online"));
	}
}
